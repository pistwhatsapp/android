package com.example.amaury.pist.loader.LoaderCallbacks;

import java.util.ArrayList;

/**
 * Created by Matthieu on 09/05/2015.
 */
public interface Callback<T> {

    public void onLoadFinished(ArrayList<T> objects, String action);
}
