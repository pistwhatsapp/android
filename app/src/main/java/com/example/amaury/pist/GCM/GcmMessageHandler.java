package com.example.amaury.pist.GCM;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.amaury.pist.PistPropre.Chat_Main;
import com.example.amaury.pist.R;
import com.example.amaury.pist.model.Message;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by Vic on 17/04/2015.
 * <p/>
 * This class defines what to do with the received message.
 * The received message will contain in its data part
 * “as we will see later in GCM server step”
 * two attributes title & message,
 * we will extract “title” value from the intent extras
 * & display it on a Toast.
 */
public class GcmMessageHandler extends IntentService {

    String titleReceived;
    private Handler handler;

    public GcmMessageHandler() {
        super("GcmMessageHandler");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
    }


    @Override
    protected void onHandleIntent(Intent intent) {

        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);


        String messageReceived = extras.getString("msg");
        titleReceived = extras.getString("title");

        Message message = new Message(true,messageReceived, titleReceived);

        if (Chat_Main.active&&message.getBinome().equals(Chat_Main.binome)) {
            updateChat(this, message);

        } else {
            try {
                notif(messageReceived);
                GcmBroadcastReceiver.completeWakefulIntent(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        //showToast();

        Log.i("GCM", "Received : (" + messageType + ")  " + extras.getString("title") + Calendar.getInstance().getTime());


    }

    private void updateChat(Context context, Message message) {

        Intent intent = new Intent("chat");
        //put whatever data you want to send, if any
        intent.putExtra("message", message);
        //send broadcast
        context.sendBroadcast(intent);
    }

    /*
    Retourne un tableau de taille 2 avec le titre en premier puis le corps du message
     */
    public String[] readString() throws JSONException {
        String[] s = new String[2];
        JSONObject obj = new JSONObject(titleReceived);

        s[0] = obj.getString("title");
        s[1] = obj.getString("msg");
        return s;
    }


    public void showToast() {
        handler.post(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(), titleReceived, Toast.LENGTH_LONG).show();
            }
        });

    }

    public void notif(String messageReceived) throws JSONException {

        // String[] s = readString();

        long[] l = {100, 1};

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(titleReceived)
                        .setContentText(messageReceived)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setVibrate(new long[]{
                                200, 100,
                                200,100,200,100});


        Intent resultIntent = new Intent(this, Chat_Main.class);

        Bundle extras = new Bundle();
        extras.putString("binome", titleReceived);
        resultIntent.putExtras(extras);

// Because clicking the notification opens a new ("special") activity, there's
// no need to create an artificial back stack.
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_ONE_SHOT
                );

        mBuilder.setContentIntent(resultPendingIntent);

        // Sets an ID for the notification
        int mNotificationId = 001;
// Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
// Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

}
