package com.example.amaury.pist.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.amaury.pist.R;
import com.example.amaury.pist.model.Message;

import java.util.List;

/**
 * Created by Matthieu on 28/04/2015.
 */
public class ChatAdapter extends ArrayAdapter<Message> {

    Context context;
    List<Message> objects;

    public ChatAdapter(Context context, int resource, List<Message> objects) {
        super(context, resource, objects);
        this.context = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int resource = R.layout.message_received;
        if (!objects.get(position).isReceived()) {
            resource = R.layout.message_sent;
        }

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();

        convertView = inflater.inflate(resource, parent, false);

        TextView message = (TextView) convertView.findViewById(R.id.message_received);

        if (!objects.get(position).isReceived()) {
            message = (TextView) convertView.findViewById(R.id.message_sent);
        }
        String messageContent = objects.get(position).getContent();
        message.setText(messageContent);
        return convertView;
    }
}
