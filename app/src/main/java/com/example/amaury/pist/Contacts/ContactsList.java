package com.example.amaury.pist.Contacts;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.amaury.pist.PistPropre.Chat_Main;
import com.example.amaury.pist.R;
import com.example.amaury.pist.adapters.ContactsListAdapter;
import com.example.amaury.pist.loader.ImplementedLoaders.AddContacts;
import com.example.amaury.pist.loader.ImplementedLoaders.GetContacts;
import com.example.amaury.pist.loader.ImplementedLoaders.GetUsers;
import com.example.amaury.pist.loader.LoaderCallbacks.Callback;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthieu on 09/05/2015.
 */
public class ContactsList extends ListActivity implements Callback<String> {

    private static final String GET_USERS = "get_users";
    private static final String GET_CONTACTS = "get_contacts";
    private static final String ADD_CONTACTS = "add_contacts";

    private static final String CONTACT_OPTIONS[] = {"Delete", "Block"}; //AJouter ici des fonctionnalités

    ProgressDialog progress;
    ListView listView;
    ListView usersDialogList;
    Handler handler;
    ArrayList<String> contacts = new ArrayList<String>();
    String user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_layout);

        handler = new Handler();

        setTitle("Contacts");

        user = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("user", "user");

        String user = PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                .getString("user", "user");
        this.progress = ProgressDialog.show(this, "Fetching...", "", false, true);
        new GetContacts(user, this, GET_CONTACTS).start();


        listView = getListView();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ContactsList.this, Chat_Main.class);
                Bundle extras = new Bundle();
                extras.putString("binome", contacts.get(position));
                intent.putExtras(extras);
                startActivity(intent);
            }
        });


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                final int positionContact = position;
                final AlertDialog manageOneContact = new AlertDialog.Builder(ContactsList.this).create();
                LayoutInflater factory = LayoutInflater.from(ContactsList.this);
                final View contactOptionsView = factory.inflate(R.layout.users_list_dialog, null);
                manageOneContact.setView(contactOptionsView);
                final ListView optionsListView = (ListView) contactOptionsView.findViewById(R.id.users_list);
                optionsListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                optionsListView.setAdapter(new ArrayAdapter<String>(ContactsList.this, R.layout.listsimple, CONTACT_OPTIONS));
                manageOneContact.setCancelable(true);
                optionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        editContact(position, positionContact);
                        manageOneContact.dismiss();
                    }
                });


                manageOneContact.show();
                return true;
            }
        });
    }

    public void editContact(int position, final int positionContact) {
        if (position == 0) {
            AsyncTask<Void, Void, Void> deleter = new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        HttpClient client = new DefaultHttpClient();
                        HttpPost post = new HttpPost("https://pist-minesnantes.rhcloud.com/deleteContact.php");
                        List<NameValuePair> paramsPost = new ArrayList<NameValuePair>();

                        String contactToDelete =  (String) listView.getAdapter().getItem(positionContact);
                        paramsPost.add(new BasicNameValuePair("user", user));
                        paramsPost.add(new BasicNameValuePair("contacts", contactToDelete));
                        Log.e("deleteContact ", contactToDelete);

                        post.setEntity(new UrlEncodedFormEntity(paramsPost));
                        HttpResponse response = client.execute(post);
                        String content = EntityUtils.toString(response.getEntity());
                        Log.e("content",content);
                        JSONArray contactsJSON = new JSONArray(content);
                        ArrayList<String> contactsList =  new ArrayList<String>();
                        for (int i = 0; i < contactsJSON.length(); i++) {
                            contactsList.add(contactsJSON.getString(i));
                        }
                        onLoadFinished(contactsList,GET_CONTACTS);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    return null;
                }


            }.execute();
            contacts.remove(positionContact);
            listView.setAdapter(new ArrayAdapter<String>(ContactsList.this,
                    R.layout.listsimple, contacts));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.contacts_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.search_contacts) {
            String user = PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                    .getString("user", "user");

            GetUsers getUsers = new GetUsers(user, this, GET_USERS, contacts);

            AlertDialog.Builder usersList = new AlertDialog.Builder(this);
            LayoutInflater factory = LayoutInflater.from(this);
            final View usersListView = factory.inflate(R.layout.users_list_dialog, null);
            usersList.setView(usersListView);
            usersDialogList = (ListView) usersListView.findViewById(R.id.users_list);
            usersDialogList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            final ArrayList<String> selectedContacts = new ArrayList<String>();

            usersDialogList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    String item = (String) usersDialogList.getAdapter().getItem(position);
                    Log.e("itemClick: ", item);
                    if (selectedContacts.contains(item)) {
                        selectedContacts.remove(item);
                        view.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                    } else {
                        selectedContacts.add(item);
                        view.setBackgroundColor(getResources().getColor(R.color.bleuselection));
                    }
                }
            });

            getUsers.start();

            usersList.setCancelable(true);
            usersList.setPositiveButton("Ajouter", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    addContacts(selectedContacts);
                }
            });

            usersList.setTitle("Utilisateurs");
            usersList.show();
        }
        return true;
    }

    @Override
    public void onLoadFinished(final ArrayList<String> objects, String action) {

        progress.dismiss();

        if (action.equals(GET_CONTACTS) || action.equals(ADD_CONTACTS)) {
            contacts = objects;
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Log.v("Contacts Fectched:", "ok");
                    listView.setAdapter(new ArrayAdapter<String>(ContactsList.this,
                            R.layout.listsimple, contacts));
                }
            });
        } else if (action.equals(GET_USERS)) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    usersDialogList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                    usersDialogList.setAdapter(new ContactsListAdapter(ContactsList.this,
                            R.layout.listcheckboxes, objects));
                }
            });
        }
    }

    public void addContacts(ArrayList<String> contacts) {
        String user = PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                .getString("user", "user");
        AddContacts addContacts = new AddContacts(user, contacts, this, ADD_CONTACTS);
        addContacts.start();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
