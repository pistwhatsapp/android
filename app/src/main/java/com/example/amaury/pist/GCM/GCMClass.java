package com.example.amaury.pist.GCM;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amaury.pist.Contacts.ContactsList;
import com.example.amaury.pist.R;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vic on 17/04/2015.
 */
public class GCMClass extends Activity {

    private final static String REGISTRATION_URL = "https://pist-minesnantes.rhcloud.com/sendtoken.php";
    private final static String PREFERENCE_REGID_KEY = "regid";


    SharedPreferences sp;

    Handler handler;

    EditText etRegId;
    TextView mailInput;
    String mail;
    GoogleCloudMessaging gcm;
    String regid;
    Button boutonValider;
    String PROJECT_NUMBER = "955348316001";


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gcm_register);
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if (sp.contains(PREFERENCE_REGID_KEY)) {
            Intent intent = new Intent(GCMClass.this, ContactsList.class);
            startActivity(intent);
        } else {
            mailInput = (TextView) findViewById(R.id.enregistrementmail);

            mailInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_SEND) {
                        registerUser();
                        handled = true;
                    }
                    return handled;
                }
            });


            boutonValider = (Button) findViewById(R.id.btnValidate);
            boutonValider.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    registerUser();
                }
            });
        }


    }

    private void registerUser() {
        mail = mailInput.getText().toString();


        new AsyncTask<Void, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(Void... params) {
                boolean success = false;
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regid = gcm.register(PROJECT_NUMBER);
                    msg = "Device registered, registration ID=" + regid;
                    Log.i("GCM", msg);

                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost(REGISTRATION_URL);

                    List<NameValuePair> requestParams = new ArrayList<NameValuePair>();
                    requestParams.add(new BasicNameValuePair("system","android"));
                    requestParams.add(new BasicNameValuePair("mail", mail));
                    requestParams.add(new BasicNameValuePair("token", regid));
                    post.setEntity(new UrlEncodedFormEntity(requestParams));

                    HttpResponse response = client.execute(post);
                    success = response.getStatusLine().getStatusCode() == 200;
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    Log.e("GCM", msg);
                }
                return success;
            }

            @Override
            protected void onPostExecute(Boolean success) {
                if (success) {
                    Toast.makeText(getApplicationContext(), "Device registered", Toast.LENGTH_LONG).show();
                    sp.edit().putString("regid", regid).commit();
                    sp.edit().putString("user", mail).commit();
                    Intent intent = new Intent(GCMClass.this, ContactsList.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Device not registered", Toast.LENGTH_LONG).show();
                }
            }
        }.execute();
    }
}