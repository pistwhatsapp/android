package com.example.amaury.pist.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Matthieu on 02/05/2015.
 */
public class Message implements Serializable{

    //to know if the message has been sent or received
    private boolean received;
    private String content;
    private String binome;

    public Message(boolean received, String content, String binome) {
        this.received = received;
        this.content = content;
        this.binome = binome;
    }


    /*
    * Constructeur pour le webservices GetMessages
    * */
    public Message(JSONObject message) throws JSONException {
        this(message.getBoolean("received"),message.getString("content"),message.getString("binome"));
    }

    public boolean isReceived() {
        return received;
    }

    public void setReceived(boolean received) {
        this.received = received;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getBinome() {
        return binome;
    }

    public void setBinome(String binome) {
        this.binome = binome;
    }


}
