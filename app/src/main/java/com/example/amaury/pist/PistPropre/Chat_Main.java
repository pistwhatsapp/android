package com.example.amaury.pist.PistPropre;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.amaury.pist.R;
import com.example.amaury.pist.adapters.ChatAdapter;
import com.example.amaury.pist.loader.ImplementedLoaders.GetMessages;
import com.example.amaury.pist.loader.LoaderCallbacks.Callback;
import com.example.amaury.pist.model.Message;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Amaury on 24/04/2015.
 */
public class Chat_Main extends ListActivity implements Callback<Message> {

    private static final String GET_MESSAGES = "get_messages";


    private ListView listView;
    private Button send;
    private EditText messageInputEditText;

    private Handler handler;

    public static String binome;

    private SharedPreferences sp;

    public static boolean active = false;

    ArrayList<Message> messages = new ArrayList<Message>();

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Message message = (Message) intent.getSerializableExtra("message");
            messages.add(message);
            listView.setAdapter(new ChatAdapter(Chat_Main.this,
                    android.R.layout.simple_list_item_1, messages));

        }
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_main);



        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        handler = new Handler();


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras.containsKey("binome")) {
            binome = extras.getString("binome");
        } else {
            binome = "matthieu.lemonnier45140@gmail.com";
        }
        setTitle(binome);

        new GetMessages(sp.getString("user", "user"), binome, this, GET_MESSAGES).start();
        // Get ListView object from xml
        listView = getListView();
        send = (Button) findViewById(R.id.sendmessage);
        messageInputEditText = (EditText) findViewById(R.id.messageInputEditText);

        // Assign adapter to ListView
        listView.setAdapter(new ChatAdapter(this, android.R.layout.simple_list_item_1, messages));

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                /*
                // ListView Clicked item index
                int itemPosition = position;

                // ListView Clicked item value
                String itemValue = (String) listView.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "Position :" + itemPosition + "  ListItem : " + itemValue, Toast.LENGTH_LONG)
                        .show();
                */
            }

        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = messageInputEditText.getText().toString();
                try {
                    sendMessage(message);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        this.registerReceiver(messageReceiver, new IntentFilter("chat"));
    }

    //Must unregister onPause()
    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(messageReceiver);
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }

    private String[] listContacts() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        Set<String> s = sp.getStringSet("contacts", new HashSet<String>());

        String[] st = new String[s.size()];

        st = s.toArray(new String[s.size()]);

        return st;
    }

    private void sendMessage(String message) throws IOException {
        new AsyncTask<String, Void, String>() {

            @Override
            protected String doInBackground(String... params) {
                try {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost("http://pist-minesnantes.rhcloud.com/messagehandler.php");

                    ArrayList<NameValuePair> postParams = new ArrayList<NameValuePair>();
                    postParams.add(new BasicNameValuePair("msg", params[0]));
                    postParams.add(new BasicNameValuePair("from", sp.getString("user", "")));
                    postParams.add(new BasicNameValuePair("mail", binome));
                    System.out.println("message sent");
                    post.setEntity(new UrlEncodedFormEntity(postParams));


                    HttpResponse response = client.execute(post);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return params[0];
            }

            @Override
            protected void onPostExecute(String result) {
                messages.add(new Message(false, result, "matthieu.lemonnier45140@gmail.com"));
                messageInputEditText.clearComposingText();
                messageInputEditText.setText("");
                listView.setAdapter(new ChatAdapter(Chat_Main.this,
                        android.R.layout.simple_list_item_1, messages));
            }
        }.execute(message);

    }

    @Override
    public void onLoadFinished(ArrayList<Message> objects, String action) {
        messages = objects;
        handler.post(new Runnable() {
            @Override
            public void run() {
                listView.setAdapter(new ChatAdapter(Chat_Main.this,
                        android.R.layout.simple_list_item_1, messages));
            }
        });
    }
}
