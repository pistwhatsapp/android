package com.example.amaury.pist.loader.ImplementedLoaders;

import com.example.amaury.pist.loader.LoaderCallbacks.Callback;
import com.example.amaury.pist.model.Message;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthieu on 09/05/2015.
 */
public class GetMessages extends Thread {

    private Callback<Message> callback;
    private String user;
    private String binome;
    private String action;

    public GetMessages(String user, String binome, Callback<Message> callback, String action){
        this.user = user;
        this.binome = binome;
        this.callback = callback;
        this.action=action;

    }


    @Override
    public void run() {
        try {
            ArrayList<Message> messages = new ArrayList<Message>();

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost("https://pist-minesnantes.rhcloud.com/getmessages.php");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
            nameValuePairs.add(new BasicNameValuePair("user", user));
            nameValuePairs.add(new BasicNameValuePair("binome", binome));
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));


            HttpResponse response = client.execute(post);

            JSONArray messagesJSON = new JSONArray(EntityUtils.toString(response.getEntity()));
            for(int i=0; i<messagesJSON.length(); i++){
                messages.add(new Message(messagesJSON.getJSONObject(i)));
            }
            callback.onLoadFinished(messages, action);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
