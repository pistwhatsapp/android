package com.example.amaury.pist.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.amaury.pist.R;
import com.example.amaury.pist.model.Contact;

import java.util.List;

/**
 * Created by Amaury on 19/05/2015.
 */

//TODO
// adapter pour la liste de contacts à ajouter


public class ListAddAdapter extends ArrayAdapter<Contact> {

    private Context context;
    private List<Contact> objects;
    private int resource;

    public ListAddAdapter(Context context, int resource, List<Contact> objects) {
        super(context, resource, objects);
        this.context = context;
        this.objects = objects;
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();

            convertView = inflater.inflate(resource, parent, false);
        }
        TextView contact = (TextView) convertView.findViewById(R.id.contact_mail);
        contact.setText(objects.get(position).getUsername());

        return convertView;
    }
}
