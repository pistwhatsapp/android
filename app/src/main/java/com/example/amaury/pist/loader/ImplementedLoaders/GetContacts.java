package com.example.amaury.pist.loader.ImplementedLoaders;

import com.example.amaury.pist.loader.LoaderCallbacks.Callback;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthieu on 10/05/2015.
 */
public class GetContacts extends Thread {

    private Callback<String> callback;
    private String user;
    private String action;

    public GetContacts(String user, Callback<String> callback, String action) {
        this.user = user;
        this.callback = callback;
        this.action = action;
    }


    @Override
    public void run() {
        try {
            ArrayList<String> contactsList = new ArrayList<String>();

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost("https://pist-minesnantes.rhcloud.com/getContacts.php");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
            nameValuePairs.add(new BasicNameValuePair("user", user));
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = client.execute(post);

            JSONArray contactsJSON = new JSONArray(EntityUtils.toString(response.getEntity()));

            for (int i = 0; i < contactsJSON.length(); i++) {
                contactsList.add(contactsJSON.getString(i));
            }
            callback.onLoadFinished(contactsList, action);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
