package com.example.amaury.pist.Contacts;

import com.example.amaury.pist.model.Contact;

import java.util.ArrayList;

/**
 * Created by Vic on 06/03/2015.
 */

public class User {
    private String username;
    private String password;
    private ArrayList<Contact> contacts;


    public User(String username, String password){
        this.username=username;
        this.password=password;
        this.setContacts();
    }

    public void setContacts() {
        ArrayList<Contact> contacts = new ArrayList<Contact>();



        this.contacts = contacts;
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword (String password) {this.password=password;}

    public String getPassword () {return this.password; }

    public String getUsername(){
        return this.username;
    }

    public void addContact(Contact contact){
        contacts.add(contact);
        updateContactOnServer();
    }

    public void deleteContact(String contactId){
        for (Contact i : contacts){
            if (i.getUsername()==contactId){
                contacts.remove(i);
            }
        }
        updateContactOnServer();
    }

    public ArrayList<Contact> getContacts(){
        return this.contacts;
    }

    public void updateContactOnServer(){
        //TODO : Envoyer les modifications apportées aux contacts sur serveur
    }

}
