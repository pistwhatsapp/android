package com.example.amaury.pist.model;

/**
 * Created by Vic on 06/03/2015.
 */
public class Contact {
    private String username;

    public Contact (String username){
        this.username=username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }



}
