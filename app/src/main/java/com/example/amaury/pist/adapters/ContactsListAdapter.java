package com.example.amaury.pist.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.amaury.pist.R;

import java.util.List;

/**
 * Created by Matthieu on 09/05/2015.
 */
public class ContactsListAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> objects;
    private int resource;


    public ContactsListAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.context = context;
        this.objects = objects;
        this.resource = resource;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();

            convertView = inflater.inflate(resource, parent, false);
        }
        TextView contactTV = (TextView) convertView.findViewById(R.id.labelC);
        contactTV.setText(objects.get(position));

        return convertView;
    }
}
